BIN=bin
SRC=src

CSC=csc
ASSEMBLYINFO="AssemblyInfo.cs"

FLAGS=/out:$(BIN)/$@.exe /target:exe -recurse:$(SRC)\$@.cs -optimize

TARGETS=cat clear cp df du kill ls mv ps pwd rm touch uname which whoami

#todo: make add to path automatically

all: $(TARGETS)

dir:
	-mkdir $(BIN)

#uname doesn't work in mono compiler
$(TARGETS): dir
	$(CSC) $(FLAGS) $(ASSEMBLYINFO)

clean: #del $(BIN)\*.exe
	del $(foreach var,$(TARGETS),$(addprefix $(BIN)\, $(addsuffix .exe, $(var))))
	rmdir $(BIN)