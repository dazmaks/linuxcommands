using System;
using System.IO;

class cat
{
	static void emp()
	{
		while(true)
		{
			Console.WriteLine(Console.ReadLine());
		}
	}
	static void print(string filename)
	{
		if (File.Exists(filename)) Console.Write(File.ReadAllText(filename));
		else Console.WriteLine("Error: file {0} not found!", filename);
	}
	static void Main(string[] args)
	{
		if (args.Length < 1)
		{
			emp();
		}
		else
		{
			for (int i = 0; i<args.Length; i++){
				if(args[i]=="--h"||args[i]=="--he"||args[i]=="--hel"||args[i]=="--help"){
					Console.WriteLine(
@"Usage: cat [OPTION]... [FILE]...
Concatenate FILE(s) to standard output.

With no FILE, or when FILE is -, read standard input.

  --test               my test option

Examples:
  cat        Copy standard input to standard output."
);
					System.Environment.Exit(0);
				}
			}
			for (int i = 0; i<args.Length; i++)
			{
				string option = args[i];
				if (option.StartsWith("-"))
				{
					switch(option){
						case "--test":
							Console.WriteLine("test option lol");
							System.Environment.Exit(0);
							break;
						default:
							if(option=="-" || option=="--"){
								emp();
								break;
							}
							if (option.StartsWith("---")) Console.WriteLine("cat: unrecognized option '{0}'\nTry 'cat --help' for more information.", option);
							else Console.WriteLine("cat: invalid option -- '{0}'\nTry 'cat --help' for more information.", option[1]);
							System.Environment.Exit(0);
							break;
					}
				}else print(option);
			}
		}
	}
}