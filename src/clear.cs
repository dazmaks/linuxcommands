using System;

class clear
{
	static void cls(){
		//TODO: make clear without scrollback
		Console.Clear();
	}
	static void help(){
		Console.WriteLine(
@"Usage: clear [options]

Options:
  -V          print curses-version
  -x          do not try to clear scrollback");
	}
	static void Main(string[] args)
	{
		if (args.Length < 1)
		{
			cls();
		}
		else
		{
			for (int i = 0; i<args.Length; i++)
			{
				string option = args[i];
				if (option.StartsWith("-"))
				{
					switch(option){
						case "-V":
							Console.WriteLine("C# version of clear command(doesn't use curses)");
							System.Environment.Exit(0);
							break;
						case "-x":
							Console.Clear();
							System.Environment.Exit(0);
							break;
						default:
							if(option=="-"){
								help();
								break;
							}else if(option=="--"){
								cls();
							}else{
								Console.WriteLine("clear: invalid option -- '{0}'", option[1]);
								help();
							}
							break;
					}
				}else cls();
			}
		}
	}
}