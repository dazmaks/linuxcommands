using System;
using System.IO;

class cp
{
	static void copy(string path, string secpath){
		File.Copy(path, secpath);
	}
	static void Main(string[] args)
	{
		if (args.Length < 1)
		{
			Console.WriteLine("cp: missing file operand\nTry 'cp --help' for more information.");
		}
		else
		{
			for (int i = 0; i<args.Length; i++){
				if(args[i]=="--h"||args[i]=="--he"||args[i]=="--hel"||args[i]=="--help"){
					Console.WriteLine(
@"Usage: cp [OPTION]... SOURCE DEST
Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.

Mandatory arguments to long options are mandatory for short options too.
  --test               my test option"
);
					System.Environment.Exit(0);
				}
			}
			if(args.Length==1){
				Console.WriteLine("cp: missing destination file operand after '{0}'\nTry 'cp --help' for more information.", args[0]);
				System.Environment.Exit(0);
			}
			for (int i = 0; i<args.Length; i++)
			{
				string option = args[i];
				if (option.StartsWith("-"))
				{
					switch(option){
						case "--test":
							Console.WriteLine("test option lol");
							System.Environment.Exit(0);
							break;
						default:
							if (option.StartsWith("---")) Console.WriteLine("cp: unrecognized option '{0}'\nTry 'cp --help' for more information.", option);
							else if(option.StartsWith("--")) Console.WriteLine("cp: unrecognized option '{0}'\nTry 'cp --help' for more information.", option);
							else Console.WriteLine("cp: missing file operand\nTry 'cp --help' for more information.");
							System.Environment.Exit(0);
							break;
					}
				}else{
					if(!args[i+1].StartsWith("-")) {
						copy(option, args[i+1]);
						i++;
					}
				}
			}
		}
	}
}