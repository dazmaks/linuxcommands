using System;
using System.IO;

class mv
{
	static void Main(string[] args)
	{
		if (args.Length > 1){
			string path = @args[0];

			for (int i = 1; i<args.Length; i++)
			{
				try
				{
					if (!File.Exists(path))
					{
						Console.WriteLine("Error: cannot stat '{0}': No such file or directory", path);
					}
					else{
						File.Move(path, args[i]);
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
			}

		}else{
			Console.WriteLine("Error: need more arguments!\nUsage: mv \"File Name\"(to read) \"File Name\"(to write)");
		}
	}
}