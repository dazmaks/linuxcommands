using System;
using System.IO;

class rm
{
	static void delete(string filename)
	{
		if (File.Exists(filename) == false)
		{
			Console.WriteLine("rm: cannot remove '{0}': No such file or directory", filename);
		}
		else
		{
			File.Delete(filename);
		}
	}
	static void Main(string[] args)
	{
		if (args.Length < 1)
		{
			Console.WriteLine("rm: missing operand\nTry 'rm --help' for more information.");
		}
		else
		{
			for (int i = 0; i<args.Length; i++){
				if(args[i]=="--h"||args[i]=="--he"||args[i]=="--hel"||args[i]=="--help"){
					Console.WriteLine(
@"Usage: rm [OPTION]... [FILE]...
Remove (unlink) the FILE(s).

  --test               my test option

To remove a file whose name starts with a '-', for example '-foo',
use one of these commands:
  rm ./-foo

  (-- -foo doesn't work now)

Note that if you use rm to remove a file, it might be possible to recover
some of its contents, given sufficient expertise and/or time.  For greater
assurance that the contents are truly unrecoverable, consider using shred."
);
					System.Environment.Exit(0);
				}
			}
			for (int i = 0; i<args.Length; i++)
			{
				string option = args[i];
				if (option.StartsWith("-"))
				{
					switch(option){
						case "--test":
							Console.WriteLine("test option lol");
							System.Environment.Exit(0);
							break;
						default:
							if(option=="--"||option=="---"){
								Console.WriteLine("rm: missing operand\nTry 'rm --help' for more information.");
								System.Environment.Exit(0);
							}
							if(option=="-"){
								delete("-");
								break;
							}
							if (option.StartsWith("--")) Console.WriteLine("rm: unrecognized option '{0}'\nTry 'rm --help' for more information.", option);
							else Console.WriteLine("rm: invalid option -- '{0}'\nTry 'rm --help' for more information.", option[1]);
							System.Environment.Exit(0);
							break;
					}
				}else if(option.StartsWith("./-")){
					string opt = option;
					opt.Substring(0, 3);
					delete(opt);
				}
				else delete(option);
			}
		}
	}
}