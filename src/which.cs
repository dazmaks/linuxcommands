using System;
using System.IO;

//works only with folders
class which
{
	static void Main(string[] args)
	{
		if (args.Length < 1)
		{
			Console.WriteLine("Error: need more arguments!\nUsage: which \"Name\"");
		}
		else
		{
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				for (int i = 0; i<args.Length; i++)
				{
					try{
						Environment.CurrentDirectory = Environment.GetEnvironmentVariable(args[i]);
						DirectoryInfo info = new DirectoryInfo(".");
						Console.WriteLine(info.FullName);
					}catch(ArgumentNullException){
						Console.WriteLine("Error: \"{0}\" isn't path", args[i]);
					}
				}
			}
		}
	}
}